package org.bootcamp.SpringBootDemo.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RestController {
	
	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	@ResponseBody
	public String hello(
			@RequestParam(value = "greeting", required = true) String greeting,
			@RequestParam(value = "tag", required = false) String tag, 
			HttpServletResponse response) {
		
		if(tag == null) {
			tag = "h1";
		}
		response.setStatus(HttpServletResponse.SC_PAYMENT_REQUIRED);
		return "<"+tag+">"+greeting+"</"+tag+">";
	}
}
